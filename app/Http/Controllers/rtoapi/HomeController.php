<?php

namespace App\Http\Controllers\rtoapi;

use App\Http\Controllers\Controller;
use Validator;
use App\Models\Vwuser;
use App\Models\Vwcategory;
use App\Models\Vwdocument;
use App\Models\Vwvehicledocument;
use App\Models\Vwvehicle;
use App\Models\Vwposter;
use App\Models\Vwslider;
use App\Models\Vwarticle;
use App\Models\Vwpage;
use App\Models\Vwotherapp;
use App\Models\Vwrenew;
use App\Models\Vwfeedback;
use App\Models\Vwnotification;
use Illuminate\Http\Request;
use DateTime;

class HomeController extends Controller
{
    public function home(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'language'          => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            // Category 
            // $categories = Vwcategory::where('language',$request->language)->get();
            $categories = Vwcategory::get();
            foreach ($categories as $cat) {
                $vehicle = Vwvehicle::where('category_id', $cat->id)->where('user_id', $request->user_id)->get();
                $cat->vehicle   = $vehicle;
            }

            // Slider
            $sliders = Vwslider::latest()->get();

            // Doucument 
            $dt = new DateTime();
            $dt = $dt->format('Y-m-d');
            // $documents = Vwdocument::where('language', $request->language)->get();
            $documents = Vwdocument::get();
            $expiring_document = '';
            foreach ($documents as $doc) {
                $date = strtotime($dt);
                if ($doc->name == 'RC') {
                    $date = strtotime("+30 day", $date);
                }
                if ($doc->name == 'PUC') {
                    $date = strtotime("+15 day", $date);
                }
                if ($doc->name == 'Insurance') {
                    $date = strtotime("+30 day", $date);
                }
                if ($doc->name == 'Fitness') {
                    $date = strtotime("+30 day", $date);
                }
                if ($doc->name == 'Permit') {
                    $date = strtotime("+30 day", $date);
                }
                if ($doc->name == 'Driving Licence') {
                    $date = strtotime("+30 day", $date);
                }
                $date = date('Y-m-d', $date);

                $vehicle_document = Vwvehicledocument::where('document_id', $doc->id)->whereHas('vehicle', function ($q) use ($request) {
                    $q->where('user_id', '=', $request->user_id);
                })->where('expiry_date', '<=', $date)->get();
                $expired_document = Vwvehicledocument::where('document_id', $doc->id)->whereHas('vehicle', function ($q) use ($request) {
                    $q->where('user_id', '=', $request->user_id);
                })->where('expiry_date', '<', $dt)->get();
                foreach ($vehicle_document as $vd) {




                    // dd($date);

                    $expiring_query = Vwvehicledocument::where('document_id', $doc->id)->whereHas('vehicle', function ($q) use ($request) {
                        $q->where('user_id', '=', $request->user_id);
                    });


                    $expiring_document = $expiring_query->where('expiry_date', '<=', $date)->where('expiry_date', '>=', $dt)->get();
                    $expiring_document = count($expiring_document);
                    if ($vd->expiry_date < $dt) {
                        $vd->expired = true;
                    } else {
                        $vd->expired = false;
                    }
                    if ($vd->expiry_date >= $dt && $vd->expiry_date <= $date) {
                        $vd->expiring = true;
                    } else {
                        $vd->expiring = false;
                    }
                    if ($vd->expiry_date > $date) {
                        $vd->present = true;
                    } else {
                        $vd->present = false;
                    }
                }

                $doc->vehicle_document = $vehicle_document;
                $doc->total     = count($vehicle_document);
                $doc->expired   = count($expired_document);
                $doc->expiring  = $expiring_document;
            }
            $re = [
                'status'    => true,
                'message'   => 'Success!',
                'data'      => [
                    'categories'  =>  $categories,
                    'sliders'    =>  $sliders,
                    'documents'  =>  $documents
                ]
            ];
        }


        return response()->json($re);
    }

    public function category()
    {
        $list = Vwcategory::latest()->get();
        $re = [
            'status'     => true,
            'message'   => 'Success',
            'data'      => $list
        ];
        return response()->json($re);
    }

    public function document()
    {
        $list = Vwdocument::latest()->get();
        $re = [
            'status'     => true,
            'message'   => 'Success',
            'data'      => $list
        ];
        return response()->json($re);
    }

    public function poster_list()
    {

        $poster = Vwposter::get();
        // dd($poster);
        // foreach($poster as $poster)
        $re = [
            'status'    => true,
            'message'   => 'Success!',
            'data'      => $poster
        ];

        return response()->json($re);
    }

    public function article_list(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'language'          => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $article = Vwarticle::where('language', $request->language)->get();
            // dd($poster);
            // foreach($poster as $poster)
            $re = [
                'status'    => true,
                'message'   => 'Success!',
                'data'      => $article
            ];
        }
        return response()->json($re);
    }

    public function articleDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'article_id'          => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $article = Vwarticle::find($request->article_id);
            // dd($poster);
            // foreach($poster as $poster)
            $re = [
                'status'    => true,
                'message'   => 'Success!',
                'data'      => $article
            ];
        }
        return response()->json($re);
    }
    public function page(Request $request, $slug)
    {
        // dd($slug);
        $validator = Validator::make($request->all(), [
            'language'          => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $page = Vwpage::where('slug', $slug)->where('language', $request->language)->first();
            // dd($poster);
            // foreach($poster as $poster)
            $re = [
                'status'    => true,
                'message'   => 'Success!',
                'data'      => $page
            ];
        }

        return response()->json($re);
    }

    public function otherApp(Request $request)
    {
        // dd($slug);

        $otherapps = Vwotherapp::get();
        // dd($poster);
        // foreach($poster as $poster)
        $re = [
            'status'    => true,
            'message'   => 'Success!',
            'data'      => $otherapps
        ];


        return response()->json($re);
    }

    public function Renew(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'language'          => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {

            $renew = Vwrenew::where('language', $request->language)->get();
            // dd($poster);
            // foreach($poster as $poster)
            $re = [
                'status'    => true,
                'message'   => 'Success!',
                'data'      => $renew
            ];
        }

        return response()->json($re);
    }

    public function add_feedback(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'          => 'required',
            'description'      => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input      = $request->all();
            $feedback   = new Vwfeedback($input);
            $feedback->save();

            // dd($poster);
            // foreach($poster as $poster)
            $re = [
                'status'    => true,
                'message'   => 'Success!',
                'data'      => $feedback
            ];
        }

        return response()->json($re);
    }

    public function addnotification(Request $request)
    {
        $dt = new DateTime();
        $dt = $dt->format('Y-m-d');

        $date = strtotime($dt);
        $date1 = strtotime("+30 day", $date);
        $date1 = date('Y-m-d', $date1);
        $thirtydays_expiry = Vwvehicledocument::where('document_id','!=',6)->where('expiry_date', '=', $date1)->get();
        // dd($thirtydays_expiry);
        foreach ($thirtydays_expiry as $key => $value) {
            $notification = new Vwnotification();
            $notification->user_id = $value->vehicle->user_id;
            $notification->message = 'your document ' . $value->document->name . ' expiry in 30 days.';
            $notification->save();
        }

        $date2 = strtotime("+15 day", $date);
        $date2 = date('Y-m-d', $date2);
        $fifteendays_expiry = Vwvehicledocument::where('document_id','!=',6)->where('expiry_date', '=', $date2)->get();
        foreach ($fifteendays_expiry as $key => $value) {
            $notification = new Vwnotification();
            $notification->user_id = $value->vehicle->user_id;
            $notification->message = 'your document ' . $value->document->name . ' expiry in 15 days.';
            $notification->save();
        }

        $date3 = strtotime("+7 day", $date);
        $date3 = date('Y-m-d', $date3);
        $sevendays_expiry = Vwvehicledocument::where('document_id','!=',6)->where('expiry_date', '=', $date3)->get();
        foreach ($sevendays_expiry as $key => $value) {
            $notification = new Vwnotification();
            $notification->user_id = $value->vehicle->user_id;
            $notification->message = 'your document ' . $value->document->name . ' expiry in 7 days.';
            $notification->save();
        }

        $date4 = strtotime("+3 day", $date);
        $date4 = date('Y-m-d', $date4);
        $threedays_expiry = Vwvehicledocument::where('document_id','!=',6)->where('expiry_date', '=', $date4)->get();
        foreach ($threedays_expiry as $key => $value) {
            $notification = new Vwnotification();
            $notification->user_id = $value->vehicle->user_id;
            $notification->message = 'your document ' . $value->document->name . ' expiry in 3 days.';
            $notification->save();
        }

        $today_expiry = Vwvehicledocument::where('document_id','!=',6)->where('expiry_date', '=', $dt)->get();
        foreach ($today_expiry as $key => $value) {
            $notification = new Vwnotification();
            $notification->user_id = $value->vehicle->user_id;
            $notification->message = 'your document ' . $value->document->name . ' expiry today.';
            $notification->save();
        }

        $re = [
            'status'    => true,
            'message'   => 'Success!',
        ];

        return response()->json($re);
    }
}
