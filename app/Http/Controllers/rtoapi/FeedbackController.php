<?php

namespace App\Http\Controllers\rtoapi;

use App\Http\Controllers\Controller;
use App\Models\Vwfeedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vwfeedback  $vwfeedback
     * @return \Illuminate\Http\Response
     */
    public function show(Vwfeedback $vwfeedback)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vwfeedback  $vwfeedback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vwfeedback $vwfeedback)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vwfeedback  $vwfeedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vwfeedback $vwfeedback)
    {
        //
    }
}
