<?php

namespace App\Http\Controllers\rtoadmin;

use App\Http\Controllers\Controller;
use App\Models\Vwdocument;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Vwdocument::latest()->get();

        $page  = 'vw_document.list';
        $title = 'Document list';
        $data  = compact('page', 'title', 'lists');

        return view('backend.layout.master', $data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page  = 'vw_document.add';
        $title = 'Add Document';
        $data  = compact('page', 'title');

        return view('backend.layout.master', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'   => 'required|string'
        ];

        $messages = [
            'record.name'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];

        $request->validate($rules, $messages);

        $record           = new Vwdocument;
        $input            = $request->record;        

        $record->fill($input);
        if ($record->save()) {
            
            return redirect(route('admin.vw_document.index'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.vw_document.index'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vwdocument  $vwdocument
     * @return \Illuminate\Http\Response
     */
    public function show(Vwdocument $vwdocument)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vwdocument  $vwdocument
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Vwdocument $vw_document)
    {
        
        $editData =  ['record' => $vw_document->toArray()];
        $request->replace($editData);
        $request->flash();

        // set page and title ------------------
        $page = 'vw_document.edit';
        $title = 'Edit Document';
        $data = compact('page', 'title', 'vw_document');

        return view('backend.layout.master', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vwdocument  $vwdocument
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vwdocument $vw_document)
    {
        $record     = $vw_document;
        $input      = $request->record;       
        

        $record->fill($input);
        if ($record->save()) {
            
            return redirect(route('admin.vw_document.index'))->with('success', 'Success! Record has been edited');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vwdocument  $vwdocument
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vwdocument $vw_document)
    {
        $vw_document->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
