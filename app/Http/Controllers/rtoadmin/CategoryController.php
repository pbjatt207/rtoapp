<?php

namespace App\Http\Controllers\rtoadmin;

use App\Http\Controllers\Controller;
use App\Models\Vwcategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Vwcategory::latest()->get();

        $page  = 'vw_category.list';
        $title = 'Category list';
        $data  = compact('page', 'title', 'lists');

        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page  = 'vw_category.add';
        $title = 'Add Category';
        $data  = compact('page', 'title');

        return view('backend.layout.master', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            // 'record.name'   => 'required|string'
        ];

        $messages = [
            'record.name'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];

        $request->validate($rules, $messages);

        $record           = new Vwcategory;
        $input            = $request->record;

        $record->fill($input);
        if ($record->save()) {

            return redirect(route('admin.vw_category.index'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.vw_category.index'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vwcategory  $vwcategory
     * @return \Illuminate\Http\Response
     */
    public function show(Vwcategory $vwcategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vwcategory  $vwcategory
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Vwcategory $vw_category)
    {

        $editData =  ['record' => $vw_category->toArray()];
        $request->replace($editData);
        $request->flash();

        // set page and title ------------------
        $page = 'vw_category.edit';
        $title = 'Edit Category';
        $data = compact('page', 'title', 'vw_category');

        return view('backend.layout.master', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vwcategory  $vwcategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vwcategory $vw_category)
    {
        $record     = $vw_category;
        $input      = $request->record;


        $record->fill($input);
        if ($record->save()) {

            return redirect(route('admin.vw_category.index'))->with('success', 'Success! Record has been edited');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vwcategory  $vwcategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vwcategory $vw_category)
    {
        $vw_category->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
