<?php

namespace App\Http\Controllers\rtoadmin;

use App\Http\Controllers\Controller;
use App\Models\Vwposter;
use Illuminate\Http\Request;

class PosterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd('ab');
        $lists = Vwposter::latest()->get();

        $page  = 'vw_poster.list';
        $title = 'Poster list';
        $data  = compact('page', 'title', 'lists');

        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $page  = 'vw_poster.add';
        $title = 'Add Poster';
        $data  = compact('page', 'title');

        return view('backend.layout.master', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'   => 'required|string'
        ];

        $messages = [
            'record.name'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];

        $request->validate($rules, $messages);

        $record           = new Vwposter;
        $input            = $request->record;        

        $record->fill($input);
        if ($record->save()) {
            if ($request->hasFile('image')) {
                $image = $request->image;
                $filename = 'IMAGE_' . sprintf('%06d', $record->id) . '.' . $image->getClientOriginalExtension();
    
                $image->storeAs('public/poster/', $filename);
                $record->image = 'poster/' . $filename;
    
                $record->image .= '?v=' . time();
                $record->save();
            }
            return redirect(route('admin.vw_poster.index'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.vw_poster.index'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vwposter  $vwposter
     * @return \Illuminate\Http\Response
     */
    public function show(Vwposter $vwposter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vwposter  $vwposter
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Vwposter $vw_poster)
    {
        $editData =  ['record' => $vw_poster->toArray()];
        $request->replace($editData);
        $request->flash();

        // set page and title ------------------
        $page = 'vw_poster.edit';
        $title = 'Edit Poster';
        $data = compact('page', 'title', 'vw_poster');

        return view('backend.layout.master', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vwposter  $vwposter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vwposter $vw_poster)
    {
        $record     = $vw_poster;
        $input      = $request->record;

        
        

        $record->fill($input);
        if ($record->save()) {
            if ($request->hasFile('image')) {
                $image = $request->image;
                $filename = 'IMAGE_' . sprintf('%06d', $record->id) . '.' . $image->getClientOriginalExtension();
    
                $image->storeAs('public/poster/', $filename);
                $record->image = 'poster/' . $filename;
    
                $record->image .= '?v=' . time();
                $record->save();
            }
            return redirect(route('admin.vw_poster.index'))->with('success', 'Success! Record has been edited');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vwposter  $vwposter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vwposter $vw_poster)
    {
        $vw_poster->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
