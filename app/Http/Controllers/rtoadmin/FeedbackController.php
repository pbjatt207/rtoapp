<?php

namespace App\Http\Controllers\rtoadmin;

use App\Http\Controllers\Controller;
use App\Models\Vwfeedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Vwfeedback::latest()->get();

        $page  = 'vw_feedback.list';
        $title = 'Feedback list';
        $data  = compact('page', 'title', 'lists');

        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vwfeedback  $vwfeedback
     * @return \Illuminate\Http\Response
     */
    public function show(Vwfeedback $vwfeedback)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vwfeedback  $vwfeedback
     * @return \Illuminate\Http\Response
     */
    public function edit(Vwfeedback $vwfeedback)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vwfeedback  $vwfeedback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vwfeedback $vwfeedback)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vwfeedback  $vwfeedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vwfeedback $vwfeedback)
    {
        //
    }
}
