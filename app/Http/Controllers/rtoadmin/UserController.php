<?php

namespace App\Http\Controllers\rtoadmin;

use App\Http\Controllers\Controller;
use App\Models\Vwuser;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Vwuser::latest()->get();

        $page  = 'vw_user.list';
        $title = 'User list';
        $data  = compact('page', 'title', 'lists');

        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vwuser  $vwuser
     * @return \Illuminate\Http\Response
     */
    public function show(Vwuser $vwuser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vwuser  $vwuser
     * @return \Illuminate\Http\Response
     */
    public function edit(Vwuser $vwuser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vwuser  $vwuser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vwuser $vwuser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vwuser  $vwuser
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vwuser $vwuser)
    {
        //
    }
}
