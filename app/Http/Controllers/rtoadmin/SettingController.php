<?php

namespace App\Http\Controllers\rtoadmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Vwsetting;

class SettingController extends Controller
{
    public function edit(Request $request, Vwsetting $setting)
    {
        $setting 	= Vwsetting::find(1);
        
        $editData =  $setting->toArray();
        $request->replace($editData);
        $request->flash();

        // set page and title ------------------
        $page = 'vw_setting.edit';
        $title = 'Setting';
        $data = compact('page', 'title', 'setting');
        // return data to view

        return view('backend.layout.master', $data);
    }

    
    public function update(Request $request)
    {
        $record     = Vwsetting::find(1);
        $record->title 		= $request->title;
        $record->tagline 	= $request->tagline;
        $record->mobile     = $request->mobile;
        $record->email      = $request->email;
         
        
        if ($record->save()) {
            if ($request->hasFile('logo')) {
                $image = $request->logo;
                // dd($image);
                $filename = 'logo.' . $image->getClientOriginalExtension();
    
                $image->storeAs('public/setting/', $filename);
                $record->logo = 'setting/' . $filename;
    
                $record->logo .= '?v=' . time();
                
            }
            if ($request->hasFile('favicon')) {
                $image = $request->favicon;
                $filename = 'favicon.' . $image->getClientOriginalExtension();
    
                $image->storeAs('public/setting/', $filename);
                $record->favicon = 'setting/' . $filename;
    
                $record->favicon .= '?v=' . time();
                
            }
            $record->save();
            return redirect()->back()->with('success', 'Success! Record has been edided');
        }
    }
}
