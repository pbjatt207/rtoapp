<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use App\Models\Vwuser;
use App\Models\Vwvehicle;
use App\Models\Vwfeedback;
use App\Models\Type;
use App\Models\Make;
use App\Models\Vwmodel;
use App\Models\VwfuelType;
use App\Models\Vwvariant;
use Str;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vwuser = Vwuser::get();
        $vwvehicle = Vwvehicle::get();
        $vwfeedback = Vwfeedback::get();

        $categories = Type::with(['subcategories' => function($q) {
            return $q->where('type_for', 'business-form');
        }])->whereNull('type_id')->select('id', 'name')->get();
        // dd($categories);


    	$page = 'homepage';
        $title = 'Master Admin Dashboard ';
        $data = compact('page','title','vwuser','vwvehicle','vwfeedback', 'categories');
        
		return view('backend.layout.master', $data);
    }

    public function select_admin(Request $request)
    {
        
    	$re = [
            'age' => $age,
            'cc' => $cc
        ];

        return response()->json($re, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function import_vehicle_master(Request $request)
    {
        // dd($request->all());
        $file = $request->file('csv_file');

        $tmpName = $file->getRealPath();
        $csvAsArray = array_map('str_getcsv', file($tmpName));
        
        $data = [];
        $field = [];

        foreach($csvAsArray as $key => $csv) {
            if($key === 0) {
                $field = $csv;
            } else {
                $data[] = array_combine($field, $csv);
            }
        }

        // dd($data);
        
        foreach( $data as $val ) {
            $make = Make::where('name', $val['Vehicle Make'])->where('type_id', $request->type_id)->first();
            if( empty($make->id) ) {
                $make = new Make();
                $make->name = $val['Vehicle Make'];
                $make->type_id = $request->type_id;
                $make->save();
            }
            
            $model = Vwmodel::where('name', $val['Vehicle Model'])->where('make_id', $make->id)->first();
            if( empty($model->id) ) {
                $model = new Vwmodel();
                $model->name = $val['Vehicle Model'];
                $model->slug = Str::slug($val['Vehicle Model']);
                $model->make_id = $make->id;
                $model->save();

                $model->slug .= '-'.$model->id;
                $model->save();
            }

            $fuel = VwfuelType::where('name', $val['Fuel'])->first();
            if( empty($fuel->id) )
            {
                $fuel = new VwfuelType();
                $fuel->name = $val['Fuel'];
                $fuel->slug = Str::slug($val['Fuel']);
                $fuel->save();

                $fuel->slug .= '-'.$fuel->id;
                $fuel->save();
            }

            $variant = Vwvariant::where('name', $val['Vehicle Sub Type'])->where('vwmodel_id', $model->id)->where('vwmfuel_type_id', $fuel->id)->first();
            if( empty($variant) ) {
                $variant = new Vwvariant();
            }
            $variant->name = $val['Vehicle Sub Type'];
            $variant->slug = Str::slug($val['Vehicle Sub Type']);
            $variant->vwmodel_id = $model->id;
            $variant->vwmfuel_type_id = $fuel->id;
            $variant->cubic_capacity = $val['Cubic Capacity'];
            $variant->seating_capacity = $val['Seating Capacity'];
            $variant->save();

            $variant->slug .= '-'.$variant->id;
            $variant->save();

            return redirect()->back();
        }
    }
}
