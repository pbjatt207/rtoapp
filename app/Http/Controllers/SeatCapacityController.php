<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SeatCapacity;
use App\Models\Type;

class SeatCapacityController extends Controller
{
    public function index()
    {
        $lists = SeatCapacity::paginate(10);

        // set page and title -------------
        $page  = 'seatcapacity.list';
        $title = 'seatcapacity list';
        $data  = compact('page', 'title', 'lists');

        return view('backend.layout.master', $data);
    }

    public function create()
    {
        $category = Type::get();
        $categoryArr  = ['' => 'Select Category'];
        if (!$category->isEmpty()) {
            foreach ($category as $pcat) {
                $categoryArr[$pcat->id] = $pcat->name;
            }
        }

        $page  = 'seatcapacity.add';
        $title = 'Add seatcapacity';
        $data  = compact('page', 'title', 'categoryArr');

        return view('backend.layout.master', $data);
    }

    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.seat_range'   => 'required|string',
            'record.type_id'   => 'required'
        ];

        $message = [
            'record.seat_range'  => 'Please Enter seat_range.',
        ];

        $request->validate($rules, $message);

        $record           = new SeatCapacity;
        $input            = $request->record;

        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.seatcapacity.index'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.seatcapacity.index'))->with('danger', 'Error! Something going wrong.');
        }
    }

    public function edit(Request $request, SeatCapacity $seatcapacity)
    {
        $category = Type::get();
        $categoryArr  = ['' => 'Select Category'];
        if (!$category->isEmpty()) {
            foreach ($category as $pcat) {
                $categoryArr[$pcat->id] = $pcat->name;
            }
        }

        $editData =  ['record' => $seatcapacity->toArray()];
        $request->replace($editData);
        $request->flash();

        $page  = 'seatcapacity.edit';
        $title = 'Edit seatcapacity';
        $data  = compact('page', 'title', 'seatcapacity', 'categoryArr');

        return view('backend.layout.master', $data);
    }

    public function update(Request $request, SeatCapacity $seatcapacity)
    {
        $record     = $seatcapacity;
        $input      = $request->record;

        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.seatcapacity.index'))->with('success', 'Success! Record has been edided');
        }
    }

    public function destroy(SeatCapacity $seatcapacity)
    {
        $seatcapacity->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
