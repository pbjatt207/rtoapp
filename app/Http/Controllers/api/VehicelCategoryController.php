<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Type;

class VehicelCategoryController extends Controller
{
    public function index()
    {
        $lists = Type::with('subcategories')->whereNull('type_id')->select('id', 'name')->get();

        return response()->json($lists);
    }
}
