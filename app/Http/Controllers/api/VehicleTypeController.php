<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Type;
use App\Models\Make;
use App\Models\Vwmodel;
use App\Models\VwfuelType;
use App\Models\VwVariant;

class VehicleTypeController extends Controller
{
    public function index()
    {
        $lists = Type::with(['subcategories' => function ($q) {
            return $q->where('type_for', 'business-form');
        }])->whereNull('type_id')->select('id', 'name')->get();

        return response()->json($lists);
    }

    public function make($vehicle_type_id)
    {
        $lists = Make::where('type_id', $vehicle_type_id)->orderBy('name')->get();

        return response()->json($lists);
    }

    public function model($make_id)
    {
        $lists = Vwmodel::where('make_id', $make_id)->orderBy('name')->get();

        return response()->json($lists);
    }

    public function fuel()
    {
        $lists = VwfuelType::orderBy('name')->get();

        return response()->json($lists);
    }

    public function variant($model_id, $fuel_id)
    {
        $lists = VwVariant::where('vwmodel_id', $model_id)->where('vwmfuel_type_id',$fuel_id)->orderBy('name')->get();

        return response()->json($lists);
    }
}
