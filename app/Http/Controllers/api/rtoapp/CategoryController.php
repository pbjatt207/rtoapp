<?php

namespace App\Http\Controllers\api\rtoapp;

use App\Http\Controllers\Controller;
use App\Models\Vwcategories;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vwcategories  $vwcategories
     * @return \Illuminate\Http\Response
     */
    public function show(Vwcategories $vwcategories)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vwcategories  $vwcategories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vwcategories $vwcategories)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vwcategories  $vwcategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vwcategories $vwcategories)
    {
        //
    }
}
