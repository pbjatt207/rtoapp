<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vwdocument extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function document()
    {
        return $this->hasMany('App\Models\Vwvehicledocument', 'document_id', 'id');
    }
}
