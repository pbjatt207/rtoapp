<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vwcategory extends Model
{
    use HasFactory;
    protected $guarded = [];

    // protected $with = ['vehicle'];

    public function vehicle()
    {
        return $this->hasMany('App\Models\Vwvehicle', 'category_id', 'id');
        // return $this->hasMany(Vwvehicle::class);
    }
}
