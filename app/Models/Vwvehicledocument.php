<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vwvehicledocument extends Model
{
    use HasFactory;
    protected $guarded = [];  
    protected $with = ['vehicle', 'document'];
    public function vehicle()
    {
        return $this->belongsTo(Vwvehicle::class);
    }
    public function document()
    {
        return $this->belongsTo(Vwdocument::class);
    }

    protected $appends = ['image_url'];

    public function getImageUrlAttribute()
    {
        return url('storage/'.$this->image);
    }
}
