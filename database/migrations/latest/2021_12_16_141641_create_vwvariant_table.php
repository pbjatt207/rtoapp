<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwvariantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vwvariant', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('slug')->unique();
            $table->unsignedInteger('cubic_capacity')->nullable();
            $table->unsignedInteger('seating_capacity')->nullable();
            $table->unsignedBigInteger('vwmodel_id');
            $table->foreign('vwmodel_id')->references('id')->on('vwmodel')->onDelete('cascade');
            $table->unsignedBigInteger('vwmfuel_type_id');
            $table->foreign('vwmfuel_type_id')->references('id')->on('vwfuel_type')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vwvariant');
    }
}
