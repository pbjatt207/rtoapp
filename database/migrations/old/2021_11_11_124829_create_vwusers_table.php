<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vwusers', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('mobile')->unique();
            $table->string('otp')->nullable();
            $table->enum('otp_verified', ['true', 'false'])->default('false');
            $table->enum('status', ['true', 'false'])->default('false');
            $table->string('referal_code')->nullable();
            $table->string('accept_code')->nullable();
            $table->enum('device_type', ['Android', 'Ios'])->nullable();
            $table->string('device_id')->nullable();
            $table->string('fcm_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vwusers');
    }
}
