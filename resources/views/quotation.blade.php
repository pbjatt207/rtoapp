<style>
    .head {
        font-weight: 400px;
    }

    table {
        width: 100%;
        font-size: 12px;
    }

    table tr th {
        border: 1px solid;
        padding: 5px;
        margin: 0;
        text-align: left;
    }

    table {
        border-collapse: collapse;
    }

    table td {
        border: 1px solid black;
        padding: 5px;
    }

    table tr:first-child td {
        border-top: 0;
    }

    table tr td:first-child {
        border-left: 0;
    }

    table tr:last-child td {
        border-bottom: 0;
    }

    table tr td:last-child {
        border-right: 0;
    }

    .footer table tr td {
        border: 0px solid;
        padding: 0;
        margin: 0;
    }
</style>
<div>
    <div class="head" style="margin-bottom: 30px;">
        <div style="width: 80%; float:left;">
            <h3>{{ $quotation->company->name }}</h3>
        </div>
        <div style="width: 20%; float:left;">
            <img src="{{ url('images/company/'. $quotation->company->image) }}" alt="" width="50%">
        </div>
    </div>

    <div style="float: right;  border: 2px 2px 2px 0px; width: 219px; padding: 2px;">

        <span>Final Premium : </span>
        <span>{{ $quotation->data->part_c_final_premium->final_premium }}</span>
    </div>
    <div style="clear:both;"></div>
    <table style="margin-bottom: 10px;">
        <tr>
            <td>Name</td>
            <td>{{ @$quotation->data->customer_detail->customer_name }}</td>
            <td>Mobile</td>
            <td>{{ @$quotation->data->customer_detail->customer_mobile }}</td>
            <td>Vehical</td>
            <td>{{ @$quotation->data->customer_detail->customer_vehicle }}</td>
        </tr>
    </table>
    <table>
        <tr>
            <td>Vehicle Make </td>
            <td></td>
            <td>Model</td>
            <td></td>
        </tr>
        <tr>
            <td>Registration No.</td>
            <td></td>
            <td>Passengers</td>
            <td></td>
        </tr>
        <tr>
            <td>Policy Type</td>
            <td>{{ @$quotation->data->basic_details->policy_type }}</td>
            <td>Type of Vehivle</td>
            <td>{{ @$quotation->data->basic_details->type_of_vehivle }}</td>
        </tr>
        <tr>
            @if($quotation->type_id == 1 && $quotation->type_id == 2 && $quotation->type_id == 7)
            <td>Vehicle's Cubic Capacity</td>
            <td>{{ @$quotation->data->basic_details->cubic_capacity }}</td>
            @endif
            @if($quotation->type_id == 3)
            <td>gvw</td>
            <td>{{ @$quotation->data->basic_details->gvw }}</td>
            @endif
            @if($quotation->type_id == 6)
            <td>Vehicle Category</td>
            <td>{{ @$quotation->data->basic_details->vehicle_cat }}</td>
            @endif
            <td>RTO</td>
            <td>{{ @$quotation->data->basic_details->trto }}</td>
        </tr>
        <tr>
            <td>Age of Vehicle</td>
            <td>{{ @$quotation->data->basic_details->age_of_vehicle }}</td>
            <td>Zone</td>
            <td>{{ @$quotation->data->basic_details->zone }}</td>
        </tr>
        @if($quotation->type_id == 7)
        <tr>
            <td>Seating Capacity</td>
            <td>{{ @$quotation->data->basic_details->seating_cap }}</td>
            <td>Carrying Capacity</td>
            <td>{{ @$quotation->data->basic_details->carrying_cap }}</td>
        </tr>
        @endif
        <tr>
            @if($quotation->type_id == 2 && $quotation->type_id == 3 && $quotation->type_id == 7)
            <td>CNG / LPG</td>
            <td>{{ @$quotation->data->basic_details->cng_lpg }}</td>
            @endif
            @if($quotation->type_id == 7)
            <td>Vehicle Category</td>
            <td>{{ @$quotation->data->basic_details->vehicle_cat }}</td>
            @endif
        </tr>
    </table>
    @if($quotation->type_id == 1)
    <table style="margin-top: 15px;">
        <tr>
            <th colspan="2">Own Damage Premium(A)</th>
            <th colspan="2">Own Damage Premium(B)</th>
        </tr>
        <tr>
            <td>Basic for vehicle</td>
            <td>{{ @$quotation->data->part_a_own_damage->vehicle_basic_rate }}</td>
            <td>Basic TP</td>
            <td>{{ $quotation->data->part_b_liability->basic_tp }}</td>
        </tr>
        <tr>
            <td>Basic Own Damage</td>
            <td>{{ $quotation->data->part_a_own_damage->basic_own_damage }}</td>
            <td>CPA Owner Driver</td>
            <td>{{ $quotation->data->part_b_liability->cpa_owner_driver }}</td>
        </tr>
        <tr>
            <td>Discount on OD premium</td>
            <td>{{ $quotation->data->part_a_own_damage->discount }}</td>
            <td>LL to Paid Driver</td>
            <td>{{ $quotation->data->part_b_liability->ll_to_paid_driver }}</td>
        </tr>
        <tr>
            <td>Loading on OD Premium</td>
            <td>{{ $quotation->data->part_a_own_damage->loading }}</td>
            <td>PA to Unnamed Passanger</td>
            <td>{{ $quotation->data->part_b_liability->pa_to_unnamed_passanger }}</td>
        </tr>
        <tr>
            <td>Basic OD after Discount/Loading</td>
            <td>{{ $quotation->data->part_a_own_damage->basic_od_after_discount }}</td>
            <td>TPPD Cover</td>
            <td>{{ $quotation->data->part_b_liability->tppd_cover }}</td>
        </tr>
        <tr>
            <td>Electrical/Electronics Accessories</td>
            <td>{{ $quotation->data->part_a_own_damage->electric_accessories_value }}</td>
            <th>Final TP</th>
            <td>{{ $quotation->data->part_b_liability->final_tp }}</td>
        </tr>
        <tr>
            <td>Non Electrical/Electronics Accessories</td>
            <td>{{ $quotation->data->part_a_own_damage->non_electric_accessories_value }}</td>
        </tr>
        <tr>
            <td>Basic OD Before NCB</td>
            <td>{{ $quotation->data->part_a_own_damage->basic_od_before_ncb }}</td>
        </tr>
        <tr>
            <td>No Claim Bonus</td>
            <td>{{ $quotation->data->part_a_own_damage->no_claim_bonus }}</td>
        </tr>
        <tr>
            <td>Gross Own Damage</td>
            <td>{{ $quotation->data->part_a_own_damage->gross_own_damage }}</td>
        </tr>
        <tr>
            <td>Zero Dep (%)</td>
            <td>{{ $quotation->data->part_a_own_damage->zero_dep }}</td>
        </tr>
        <tr>
            <td>Other Add-On (%)</td>
            <td>{{ $quotation->data->part_a_own_damage->other_add_on_percent }}</td>
        </tr>
        <tr>
            <td>Other Add-On</td>
            <td>{{ $quotation->data->part_a_own_damage->other_add_on }}</td>
        </tr>
        <tr style="margin-top: 5px;">
            <th>Net Own Damage Premium (A) </th>
            <td>{{ $quotation->data->part_a_own_damage->final_own_damage }}</td>
        </tr>
    </table>
    @endif
    @if($quotation->type_id == 2)
    <table style="margin-top: 15px;">
        <tr>
            <th colspan="2">Own Damage Premium(A)</th>
            <th colspan="2">Own Damage Premium(B)</th>
        </tr>
        <tr>
            <td>Basic for vehicle</td>
            <td>{{ $quotation->data->part_a_own_damage->vehicle_basic_rate }}</td>
            <td>Basic TP</td>
            <td>{{ $quotation->data->part_b_liability->basic_tp }}</td>
        </tr>
        <tr>
            <td>Basic Own Damage</td>
            <td>{{ $quotation->data->part_a_own_damage->basic_own_damage }}</td>
            <td>bi_fuel_system</td>
            <td>{{ $quotation->data->part_b_liability->bi_fuel_system }}</td>
        </tr>
        <tr>
            <td>Discount on OD premium</td>
            <td>{{ $quotation->data->part_a_own_damage->discount }}</td>
            <td>CPA Owner Driver</td>
            <td>{{ $quotation->data->part_b_liability->cpa_owner_driver }}</td>
        </tr>
        <tr>
            <td>Loading on OD Premium</td>
            <td>{{ $quotation->data->part_a_own_damage->loading }}</td>
            <td>LL to Paid Driver</td>
            <td>{{ $quotation->data->part_b_liability->ll_to_paid_driver }}</td>
        </tr>
        <tr>
            <td>Basic OD after Discount/Loading</td>
            <td>{{ $quotation->data->part_a_own_damage->basic_od_after_discount }}</td>
            <td>PA to Unnamed Passanger</td>
            <td>{{ $quotation->data->part_b_liability->pa_to_unnamed_passanger }}</td>
        </tr>
        <tr>
            <td>CNG / LPG Value</td>
            <td>{{ $quotation->data->part_a_own_damage->cng_lpg_value }}</td>
            <td>TPPD Cover</td>
            <td>{{ $quotation->data->part_b_liability->tppd_cover }}</td>
        </tr>
        <tr>
            <td>Electrical/Electronics Accessories</td>
            <td>{{ $quotation->data->part_a_own_damage->electric_accessories_value }}</td>
            <th>Final TP</th>
            <td>{{ $quotation->data->part_b_liability->final_tp }}</td>
        </tr>
        <tr>
            <td>Non Electrical/Electronics Accessories</td>
            <td>{{ $quotation->data->part_a_own_damage->non_electric_accessories_value }}</td>
        </tr>
        <tr>
            <td>Basic OD Before NCB</td>
            <td>{{ $quotation->data->part_a_own_damage->basic_od_before_ncb }}</td>
        </tr>
        <tr>
            <td>No Claim Bonus</td>
            <td>{{ $quotation->data->part_a_own_damage->no_claim_bonus }}</td>
        </tr>
        <tr>
            <td>Gross Own Damage</td>
            <td>{{ $quotation->data->part_a_own_damage->gross_own_damage }}</td>
        </tr>
        <tr>
            <td>Zero Dep (%)</td>
            <td>{{ $quotation->data->part_a_own_damage->zero_dep }}</td>
        </tr>
        <tr>
            <td>rsa</td>
            <td>{{ $quotation->data->part_a_own_damage->rsa }}</td>
        </tr>
        <tr>
            <td>engine_pro</td>
            <td>{{ $quotation->data->part_a_own_damage->engine_pro }}</td>
        </tr>
        <tr>
            <td>return_invoice</td>
            <td>{{ $quotation->data->part_a_own_damage->return_invoice }}</td>
        </tr>
        <tr>
            <td>cousumables</td>
            <td>{{ $quotation->data->part_a_own_damage->cousumables }}</td>
        </tr>
        <tr>
            <td>ncb_pro</td>
            <td>{{ $quotation->data->part_a_own_damage->ncb_pro }}</td>
        </tr>
        <tr>
            <td>tyre_cover</td>
            <td>{{ $quotation->data->part_a_own_damage->tyre_cover }}</td>
        </tr>
        <tr>
            <td>Other Add-On (%)</td>
            <td>{{ $quotation->data->part_a_own_damage->other_add_on_percent }}</td>
        </tr>
        <tr>
            <td>Other Add-On</td>
            <td>{{ $quotation->data->part_a_own_damage->other_add_on }}</td>
        </tr>
        <tr style="margin-top: 5px;">
            <th>Net Own Damage Premium (A) </th>
            <td>{{ $quotation->data->part_a_own_damage->final_own_damage }}</td>
        </tr>
    </table>
    @endif
    @if($quotation->type_id == 3)
    <table style="margin-top: 15px;">
        <tr>
            <th colspan="2">Own Damage Premium(A)</th>
            <th colspan="2">Own Damage Premium(B)</th>
        </tr>
        <tr>
            <td>Basic for vehicle</td>
            <td>{{ $quotation->data->part_a_own_damage->vehicle_basic_rate }}</td>
            <td>Basic TP</td>
            <td>{{ $quotation->data->part_b_liability->basic_tp }}</td>
        </tr>
        <tr>
            <td>Basic Own Damage</td>
            <td>{{ $quotation->data->part_a_own_damage->basic_own_damage }}</td>
            <td>cng_amt</td>
            <td>{{ $quotation->data->part_b_liability->cng_amt }}</td>
        </tr>
        <tr>
            <td>CNG / LPG Value</td>
            <td>{{ $quotation->data->part_a_own_damage->cng_lpg_value }}</td>
            <td>geo_tp</td>
            <td>{{ $quotation->data->part_b_liability->geo_tp }}</td>
        </tr>
        <tr>
            <td>Electrical/Electronics Accessories</td>
            <td>{{ $quotation->data->part_a_own_damage->electric_accessories_value }}</td>
            <td>CPA Owner Driver</td>
            <td>{{ $quotation->data->part_b_liability->cpa_owner_driver }}</td>
        </tr>
        <tr>
            <td>geo_ext</td>
            <td>{{ $quotation->data->part_a_own_damage->geo_ext }}</td>
            <td>LL to Paid Driver</td>
            <td>{{ $quotation->data->part_b_liability->ll_to_paid_driver }}</td>
        </tr>
        <tr>
            <td>imt</td>
            <td>{{ $quotation->data->part_a_own_damage->imt }}</td>
            <td>ll_employee</td>
            <td>{{ $quotation->data->part_b_liability->ll_employee }}</td>
        </tr>
        <tr>
            <td>Discount on OD premium</td>
            <td>{{ $quotation->data->part_a_own_damage->discount }}</td>
            <th>pa_paid</th>
            <td>{{ $quotation->data->part_b_liability->pa_paid }}</td>
        </tr>
        <tr>
            <td>Loading on OD Premium</td>
            <td>{{ $quotation->data->part_a_own_damage->loading }}</td>
            <td>TPPD Cover</td>
            <td>{{ $quotation->data->part_b_liability->tppd_cover }}</td>
        </tr>
        <tr>
            <td>Basic OD after Discount/Loading</td>
            <td>{{ $quotation->data->part_a_own_damage->basic_od_after_discount }}</td>
            <th>Final TP</th>
            <td>{{ $quotation->data->part_b_liability->final_tp }}</td>
        </tr>
        <tr>
            <td>Basic OD Before NCB</td>
            <td>{{ $quotation->data->part_a_own_damage->basic_od_before_ncb }}</td>
        </tr>
        <tr>
            <td>No Claim Bonus</td>
            <td>{{ $quotation->data->part_a_own_damage->no_claim_bonus }}</td>
        </tr>
        <tr>
            <td>Gross Own Damage</td>
            <td>{{ $quotation->data->part_a_own_damage->gross_own_damage }}</td>
        </tr>
        <tr>
            <td>Zero Dep (%)</td>
            <td>{{ $quotation->data->part_a_own_damage->zero_dep }}</td>
        </tr>
        <tr>
            <td>rsa</td>
            <td>{{ $quotation->data->part_a_own_damage->rsa }}</td>
        </tr>
        <tr>
            <td>Other Add-On (%)</td>
            <td>{{ $quotation->data->part_a_own_damage->other_add_on_percent }}</td>
        </tr>
        <tr>
            <td>Other Add-On</td>
            <td>{{ $quotation->data->part_a_own_damage->other_add_on }}</td>
        </tr>
        <tr style="margin-top: 5px;">
            <th>Net Own Damage Premium (A) </th>
            <td>{{ $quotation->data->part_a_own_damage->final_own_damage }}</td>
        </tr>
    </table>
    @endif
    @if($quotation->type_id == 7)
    <table style="margin-top: 15px;">
        <tr>
            <th colspan="2">Own Damage Premium(A)</th>
            <th colspan="2">Own Damage Premium(B)</th>
        </tr>
        <tr>
            <td>Basic for vehicle</td>
            <td>{{ $quotation->data->part_a_own_damage->vehicle_basic_rate }}</td>
            <td>Basic TP</td>
            <td>{{ $quotation->data->part_b_liability->basic_tp }}</td>
        </tr>
        <tr>
            <td>Basic Own Damage</td>
            <td>{{ $quotation->data->part_a_own_damage->basic_own_damage }}</td>
            <td>passenger_tp</td>
            <td>{{ $quotation->data->part_b_liability->passenger_tp }}</td>
        </tr>
        <tr>
            <td>CNG / LPG Value</td>
            <td>{{ $quotation->data->part_a_own_damage->cng_lpg_value }}</td>
            <td>bi_fuel_system</td>
            <td>{{ $quotation->data->part_b_liability->bi_fuel_system }}</td>
        </tr>
        <tr>
            <td>Electrical/Electronics Accessories</td>
            <td>{{ $quotation->data->part_a_own_damage->electric_accessories_value }}</td>
            <td>geo_tp</td>
            <td>{{ $quotation->data->part_b_liability->geo_tp }}</td>
        </tr>
        <tr>
            <td>geo_ext</td>
            <td>{{ $quotation->data->part_a_own_damage->geo_ext }}</td>
            <td>CPA Owner Driver</td>
            <td>{{ $quotation->data->part_b_liability->cpa_owner_driver }}</td>
        </tr>
        <tr>
            <td>imt</td>
            <td>{{ $quotation->data->part_a_own_damage->imt }}</td>
            <td>LL to Paid Driver</td>
            <td>{{ $quotation->data->part_b_liability->ll_to_paid_driver }}</td>
        </tr>
        <tr>
            <td>Discount on OD premium</td>
            <td>{{ $quotation->data->part_a_own_damage->discount }}</td>
            <td>ll_employee</td>
            <td>{{ $quotation->data->part_b_liability->ll_employee }}</td>
        </tr>
        <tr>
            <td>Loading on OD Premium</td>
            <td>{{ $quotation->data->part_a_own_damage->loading }}</td>
            <th>pa_paid</th>
            <td>{{ $quotation->data->part_b_liability->pa_paid }}</td>
        </tr>
        <tr>
            <td>Basic OD after Discount/Loading</td>
            <td>{{ $quotation->data->part_a_own_damage->basic_od_after_discount }}</td>
            <td>TPPD Cover</td>
            <td>{{ $quotation->data->part_b_liability->tppd_cover }}</td>
        </tr>
        <tr>
            <td>Basic OD Before NCB</td>
            <td>{{ $quotation->data->part_a_own_damage->basic_od_before_ncb }}</td>
            <th>Final TP</th>
            <td>{{ $quotation->data->part_b_liability->final_tp }}</td>
        </tr>
        <tr>
            <td>No Claim Bonus</td>
            <td>{{ $quotation->data->part_a_own_damage->no_claim_bonus }}</td>
        </tr>
        <tr>
            <td>Gross Own Damage</td>
            <td>{{ $quotation->data->part_a_own_damage->gross_own_damage }}</td>
        </tr>
        <tr>
            <td>Zero Dep (%)</td>
            <td>{{ $quotation->data->part_a_own_damage->zero_dep }}</td>
        </tr>
        <tr>
            <td>rsa</td>
            <td>{{ $quotation->data->part_a_own_damage->rsa }}</td>
        </tr>
        <tr>
            <td>Other Add-On (%)</td>
            <td>{{ $quotation->data->part_a_own_damage->other_add_on_percent }}</td>
        </tr>
        <tr>
            <td>Other Add-On</td>
            <td>{{ $quotation->data->part_a_own_damage->other_add_on }}</td>
        </tr>
        <tr style="margin-top: 5px;">
            <th>Net Own Damage Premium (A) </th>
            <td>{{ $quotation->data->part_a_own_damage->final_own_damage }}</td>
        </tr>
    </table>
    @endif
    @if($quotation->type_id == 6)
    <table style="margin-top: 15px;">
        <tr>
            <th colspan="2">Own Damage Premium(A)</th>
            <th colspan="2">Own Damage Premium(B)</th>
        </tr>
        <tr>
            <td>Basic for vehicle</td>
            <td>{{ $quotation->data->part_a_own_damage->vehicle_basic_rate }}</td>
            <td>Basic TP</td>
            <td>{{ $quotation->data->part_b_liability->basic_tp }}</td>
        </tr>
        <tr>
            <td>Basic Own Damage</td>
            <td>{{ $quotation->data->part_a_own_damage->basic_own_damage }}</td>
            <td>CPA Owner Driver</td>
            <td>{{ $quotation->data->part_b_liability->cpa_owner_driver }}</td>
        </tr>
        <tr>
            <td>Electrical/Electronics Accessories</td>
            <td>{{ $quotation->data->part_a_own_damage->electric_accessories_value }}</td>
            <td>LL to Paid Driver</td>
            <td>{{ $quotation->data->part_b_liability->ll_to_paid_driver }}</td>
        </tr>
        <tr>
            <td>imt_fs</td>
            <td>{{ $quotation->data->part_a_own_damage->imt_fs }}</td>
            <td>ll_employee</td>
            <td>{{ $quotation->data->part_b_liability->ll_employee }}</td>
        </tr>
        <tr>
            <td>imt</td>
            <td>{{ $quotation->data->part_a_own_damage->imt }}</td>
            <td>TPPD Cover</td>
            <td>{{ $quotation->data->part_b_liability->tppd_cover }}</td>
        </tr>
        <tr>
            <td>discount_before</td>
            <td>{{ $quotation->data->part_a_own_damage->discount_before }}</td>
            <th>Final TP</th>
            <td>{{ $quotation->data->part_b_liability->final_tp }}</td>
        </tr>
        <tr>
            <td>Discount on OD premium</td>
            <td>{{ $quotation->data->part_a_own_damage->discount }}</td>
        </tr>
        <tr>
            <td>Loading on OD Premium</td>
            <td>{{ $quotation->data->part_a_own_damage->loading }}</td>
        </tr>
        <tr>
            <td>Basic OD after Discount/Loading</td>
            <td>{{ $quotation->data->part_a_own_damage->basic_od_after_discount }}</td>
        </tr>
        <tr>
            <td>No Claim Bonus</td>
            <td>{{ $quotation->data->part_a_own_damage->no_claim_bonus }}</td>
        </tr>
        <tr>
            <td>Gross Own Damage</td>
            <td>{{ $quotation->data->part_a_own_damage->gross_own_damage }}</td>
        </tr>
        <tr>
            <td>Zero Dep (%)</td>
            <td>{{ $quotation->data->part_a_own_damage->zero_dep }}</td>
        </tr>
        <tr>
            <td>Other Add-On (%)</td>
            <td>{{ $quotation->data->part_a_own_damage->other_add_on_percent }}</td>
        </tr>
        <tr>
            <td>Other Add-On</td>
            <td>{{ $quotation->data->part_a_own_damage->other_add_on }}</td>
        </tr>
        <tr style="margin-top: 5px;">
            <th>Net Own Damage Premium (A) </th>
            <td>{{ $quotation->data->part_a_own_damage->final_own_damage }}</td>
        </tr>
    </table>
    @endif
    <table style="margin-top: 15px;">
        <tr></tr>
        <tr>
            <th>Premium before GST (A+B+C)</th>
            <th>{{ $quotation->data->part_c_final_premium->premium_before_taxes }}</th>
        </tr>
        @if($quotation->type_id == 1 && $quotation->type_id == 2 && $quotation->type_id == 6)
        <tr>
            <th>GST @18% </th>
            <th>{{ $quotation->data->part_c_final_premium->gst }}</th>
        </tr>
        @endif
        @if($quotation->type_id == 3)
        <tr>
            <th>GST @12% </th>
            <th>{{ $quotation->data->part_c_final_premium->gst_twelve }}</th>
        </tr>
        <tr>
            <th>GST @18% </th>
            <th>{{ $quotation->data->part_c_final_premium->gst_eighteen }}</th>
        </tr>
        @endif
        @if($quotation->type_id == 7)
        <tr>
            <th>GST @18% </th>
            <th>{{ $quotation->data->part_c_final_premium->gst_eighteen }}</th>
        </tr>
        @endif
        <tr>
            <th>Other Cess</th>
            <th>{{ $quotation->data->part_c_final_premium->kerala_cess }}</th>
        </tr>
        <tr>
            <th>Final Premium </th>
            <th>{{ $quotation->data->part_c_final_premium->final_premium }}</th>
        </tr>
    </table>

    <div style="padding-top: 5px;">
        <div style="padding-top: 7px;">
            <p style="margin: 0px; padding: 0px; font-size: 13px;">Kindly pay cheque/DD in favor of</p>
            <h4 style="margin-top: 0;">{{ $quotation->company->name }}</h4>
        </div>
        <div>
            <p style="margin: 0px; padding: 0px; font-size: 13px;">Documents Required:-</p>
            <table>
                <tr>
                    <td>1. Previous Policy Copy</td>
                    <td>2. RC copy</td>
                    <td>3. Other as requirement</td>
                </tr>
            </table>
            <p style="font-size: 13px; font-weight: bold;">Note : In case of any claim, NCB will be revised and hence Quotation is Subject to Change.</p>
            <p style="font-size: 13px; font-weight: bold;">Quote Validity: This Quote is valid till midnight</p>
            <p style="text-align:center; font-size: 12px;">Insurance is subject matter of the solicitation.</p>
        </div>
        <div class="footer">
            <table>
                <tr>
                    <td rowspan="3" style="width: 25%;">
                        @if($user->image)
                        <img src="{{ url('/').'/images/profile/'.$user->image }}" />
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Adviser Name</td>
                    <td>:</td>
                    <td>{{ $user->sign->name }}</td>
                    <td>Adviser Contact</td>
                    <td>:</td>
                    <td>{{ $user->sign->mobile }}</td>
                </tr>
                <tr>
                    <td>Adviser Mail</td>
                    <td>:</td>
                    <td>{{ $user->sign->email }}</td>
                    <td>Quote Date</td>
                    <td>:</td>
                    <td>{{ $quotation->date }}</td>
                </tr>
            </table>
        </div>
    </div>