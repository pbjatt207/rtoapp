@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif

@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="row">
  <div class="col-lg-6">
    {{Form::text('record[name]', '', ['class' => 'squareInput', 'placeholder'=>'Enter name','required'=>'required'])}}
    {{Form::label('record[name]', 'Enter name'), ['class' => 'active']}}
  </div>

  <div class="col-lg-6">
    {{Form::text('record[h_name]', '', ['class' => 'squareInput', 'placeholder'=>'Enter hindi name','required'=>'required'])}}
    {{Form::label('record[h_name]', 'Enter hindi name'), ['class' => 'active']}}
  </div>
  <!-- @php
$langArr = [
  'en' => 'English',
  'hi' => 'Hindi'
  ];
@endphp
  <div class="col-lg-6">
    {{ Form::label('language', 'Select Language'), ['class' => 'active'] }}
    {{ Form::select('record[language]', $langArr,'0', ['class'=>'form-control', 'style'=> 'border:1px solid;','required'=>'required']) }}
  </div> -->
</div>