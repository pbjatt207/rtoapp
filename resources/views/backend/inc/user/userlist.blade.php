<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">
                        <li class="breadcrumb-item">
                            <h4 class="page-title">User List</h4>
                        </li>
                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{ route('admin-home') }}">
                                <i class="fas fa-home"></i> Home</a>
                        </li>
                        <li class="breadcrumb-item active">User List</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2 class="float-left"><b>User List</b></h2>
                            <form method="get" class="float-right" style="display: flex;">
                                <select name="limit" class="form-control mr-4">
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="200">200</option>
                                    <option value="500">500</option>
                                </select>
                                <input name="s" type="text" class="form-control mr-4" placeholder="Search......">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                    </div>
                    <div class="body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>User Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>Address</th>
                                        <th>Referal Code</th>
                                        <th>Referal User</th>
                                        <th>Register Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $sn = $lists->firstItem();
                                    @endphp
                                    @foreach($lists as $list)
                                    <tr>
                                        <td>{{ $sn++ }}</td>
                                        <td>{{ $list->name }}</td>
                                        <td>{{ $list->user_name }}</td>
                                        <td>{{ $list->email }}</td>
                                        <td>{{ $list->mobile }}</td>
                                        @if($list->address_id)
                                        <td>{{ $list->address->village }}, {{ $list->address->postoffice }},<br>{{ $list->address->district }} ({{ $list->address->state }}), {{ $list->address->pincode }}</td>
                                        @else
                                        <td>N/A</td>
                                        @endif
                                        <td>
                                            <span>{{ $list->referal_code }}</span> 
                                           <!-- &nbsp;-->
                                           <!--<i class="fas fa-copy copyText" data-value="{{ $list->referal_code }}"></i>-->
                                        </td>
                                        <td>
                                            @if($list->accept_code != '' && $list->accept_code != null)
                                            {{$list->referal_user->name}}
                                            @else
                                            <span>N/A</span>
                                            @endif
                                        </td>
                                        <td>
                                            {{ date('d M Y h:i A', strtotime($list->created_at)) }}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
        <div class="paginate" id="paginate">
                                {{$lists->appends(Request::all())->links("pagination::bootstrap-4")}}
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>