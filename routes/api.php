<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'api'], function () {
    Route::get('/rto', 'RtoController@index');
    Route::get('/address', 'AddressController@index');
    Route::get('/city', 'AddressController@city');
    Route::get('/age', 'AgeController@index');
    Route::get('/cc_model', 'CubicCapacityController@index');
    Route::get('/seat_capacity', 'SeatCapacityController@index');
    Route::get('/weight_capacity', 'WeightCapacityController@index');
    Route::post('/idv_rate', 'IdvrateController@index');
    Route::post('/price', 'PriceController@index');

    Route::get('/type', 'TypeController@index');


    Route::get('/vehicle-type', 'VehicleTypeController@index');
    Route::get('/make/{type_id}', 'VehicleTypeController@make');
    Route::get('/model/{make_id}', 'VehicleTypeController@model');
    Route::get('/fuel', 'VehicleTypeController@fuel');
    Route::get('/variant/{model_id}/{fuel_type_id}', 'VehicleTypeController@variant');

    Route::get('/healthplanprice', 'HealthController@healthplanprice');
    Route::get('/familysize', 'HealthController@familysize');
    Route::get('/healthplan', 'HealthController@healthplan');
    Route::get('/healthage', 'HealthController@healthage');
    Route::get('/healthzone', 'HealthController@healthzone');
    Route::get('/company', 'HealthController@company');

    Route::get('/check_username/{name}', 'RegisterController@checkusername');
    Route::post('/register', 'RegisterController@register');
    Route::post('/otp_verify', 'RegisterController@verifyotp');
    Route::post('/mail_verify', 'RegisterController@verifymail');
    Route::post('/send_otp', 'RegisterController@sendotp');
    Route::post('/send_otp_mail', 'RegisterController@sendotpmail');

    Route::post('/login', 'LoginController@login');
    Route::post('/forgotpassword', 'LoginController@forgotpassword');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['namespace' => 'api'], function () {

    Route::get('/slider', 'SliderController@index');
    Route::get('/home', 'HomeController@index');
});
Route::group(['namespace' => 'api', 'middleware' => 'auth:api'], function () {

    Route::get('/logout', 'LoginController@logout');

    Route::get('/signature', 'SignatureController@index');
    Route::post('/signature', 'SignatureController@update');

    Route::post('/profile', 'UserController@profile');

    Route::get('/quotation', 'QuotationController@index');
    Route::post('/quotation', 'QuotationController@store');
    Route::get('/quotation/pdf/{id}', 'QuotationController@quotation_pdf');
});


Route::group(['prefix' => 'rtoapp', 'namespace' => 'rtoapi'], function () {

    Route::get('/addnotification', 'HomeController@addnotification');

    Route::post('/sendOtp', 'UserController@sendOtp');
    Route::post('/login', 'UserController@login');
    Route::get('/profile/{id}', 'UserController@profile');
    Route::get('/logout', 'UserController@logout');
    Route::get('/home', 'HomeController@home');
    Route::get('/category', 'HomeController@category');
    Route::get('/document', 'HomeController@document');
    Route::get('/vehicles', 'VehicleController@index');
    Route::get('/vehicle_detail/{id}', 'VehicleController@vehicle_detail');
    Route::post('/add_vehicle', 'VehicleController@store');
    Route::post('/add_license', 'VehicleController@add_license');
    Route::get('/poster_list', 'HomeController@poster_list');
    Route::get('/article_list', 'HomeController@article_list');
    Route::get('/article_detail', 'HomeController@articleDetail');
    Route::get('/page/{slug}', 'HomeController@page');
    Route::get('/other_app', 'HomeController@otherApp');
    Route::get('/renew', 'HomeController@Renew');
    Route::post('/editProfile', 'UserController@editProfile');
    Route::post('/add_feedback', 'HomeController@add_feedback');
});
